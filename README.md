# Python Julia Sets

## Description

This is a Python program with a simple GUI that computes and plots the (filled)
Julia set of the quadratic polynomial `f(z) = z^2 + c` where `z` and `c` are
complex numbers. It uses `pygame` for the GUI and `scipy` for most of the
computations.

## Installation

To install you will need `pip` and `pipenv` installed. In the root directory of
the project run the command `pipenv sync` to automatically install the correct
version of python and all of the dependencies to a virtual environment. Then, to
run the program, run the command `pipenv run python main.py`.

## Usage

The main window will display two images: to the left is an image of the (filled)
Julia set for `c = -0.123 + 0.745i` and to the right is the Mandelbrot set. You
can control the display of these sets using the mouse. Left clicking on any set
recenters the image around the clicked point. Using the scroll wheel will zoom
in and out of the images. Finally, right clicking on a point in the Mandelbrot
set will select that point as the new parameter `c` and redraw the Julia set for
that `c`. You can exit the program by pressing the `escape` key on the keyboard.

## Branches

The `master` branch does not use any multiprocessing capabilities. There is
another branch called `multiprocess` which uses Python's `multiprocessing`
library to achieve speedups using multiprocessing. The `multiprocess` branch is
recommended if you have a CPU with at least two cores.
