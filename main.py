#! /bin/python3

# Copyright 2018 Dan Lilja

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if __name__ == "__main__":
    import pygame as pg
    import scipy as sp
    from math import sqrt, cos, sin, pi, floor

    # Global variables
    FPS = 10
    WINDOW_SIZE = [1200, 600]
    M_SIZE = [500, 500]
    J_SIZE = [500, 500]
    J_ZOOM = 1
    M_ZOOM = 1
    J_CENTER = 0
    M_CENTER = 0
    J_ITER = 100 # Number of iterations for Julia set computation
    M_ITER = 100 # Number of iterations for Mandelbrot set computation
    C = -0.123 + 0.745j

    # Helper functions

    def compute_julia(array, c, max_iter):
        """
        Function for computing the Julia set of the map f(z) = z^2 + c where c is a
        given complex number. Given a scipy.array of size n by m of complex numbers
        it returns a scipy.array of the same size filled with ones and zeros
        corresponding to whether the point belongs to the Julia set or not
        respectively.
        """
        for it in range(max_iter):
            not_done = sp.less(array.real * array.real + array.imag * array.imag, 4)
            array[not_done] = array[not_done] * array[not_done] + c
        return not_done

    def compute_mandelbrot(array, max_iter):
        """
        Function for computing the Mandelbrot set. Given a scipy.array of n by m complex
        numbers it returns a scipy.array of the same size filled with ones and zeros
        corresponding to whether the point belongs to the mandelbrot set or not
        respectively.
        """
        values = sp.zeros(array.shape)*1j
        for it in range(max_iter):
            not_done = sp.less(values.real * values.real + values.imag * values.imag, 4)
            values[not_done] = values[not_done] * values[not_done] + array[not_done]
        return not_done

    def draw_julia(screen, surf):
        """
        Function for drawing the Julia set onto the surface surf, blitting the
        surface to the screen and finally flipping the screen to display the
        Julia set.
        """
        j_image.fill(pg.Color("red"))
        j_array = create_array(J_CENTER, J_ZOOM, J_SIZE)
        j_array = sp.linspace(-2 / J_ZOOM, 2 / J_ZOOM, J_SIZE[0])[:, sp.newaxis] \
            + sp.linspace(2 / J_ZOOM, -2 / J_ZOOM, J_SIZE[0]) * 1j
        j_set = compute_julia(j_array, C, J_ITER)
        j_pixelarray = pg.surfarray.array2d(j_image)
        j_pixelarray[j_set] = 0
        pg.surfarray.blit_array(surf, j_pixelarray)
        screen.blit(surf, (50,50))
        pg.display.flip()

    def draw_mandelbrot(screen, surf):
        """
        Function for drawing the Mandelbrot set onto the surface surf, blitting the
        surface to the screen and finally flipping the screen to display the
        Mandelbrot set.
        """
        m_image.fill(pg.Color("green"))
        m_array = create_array(M_CENTER, M_ZOOM, M_SIZE)
        m_set = compute_mandelbrot(m_array, M_ITER)
        m_pixelarray = pg.surfarray.array2d(m_image)
        m_pixelarray[m_set] = 0
        pg.surfarray.blit_array(surf, m_pixelarray)
        screen.blit(surf, (650,50))
        pg.display.flip()

    def create_array(center, zoom, size):
        """
        Function to create the array of complex numbers c corresponding to a
        rectangle of size size, centered at the point center, zoomed in
        uniformly by a factor zoom
        """
        return sp.linspace(-2 / zoom + center.real, 2 / zoom + center.real, size[0])[:, sp.newaxis] \
            + sp.linspace(2 / zoom + center.imag, -2 / zoom + center.imag, size[0]) * 1j

    # Main function

    # Initating

    pg.init()
    screen = pg.display.set_mode(WINDOW_SIZE)
    screen.fill(pg.Color("white"))

    # Creating surfaces for the Mandelbrot and Julia sets

    j_image = pg.Surface(J_SIZE)
    j_image.fill(pg.Color("red"))
    m_image = pg.Surface(M_SIZE)
    m_image.fill(pg.Color("green"))

    # Drawing the sets

    draw_julia(screen, j_image)
    draw_mandelbrot(screen, m_image)

    running = True
    clock = pg.time.Clock()

    pg.display.flip()

    while running:
        clock.tick(FPS)
        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False
            if event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE:
                running = False

            # Getting the rects for the Mandelbrot and Julia sets

            j_rect = j_image.get_rect(top = 50, left = 50)
            m_rect = m_image.get_rect(top = 50, left = 650)
            mouse_pos = pg.mouse.get_pos()

            # Handling inputs

            if event.type == pg.MOUSEBUTTONUP and j_rect.collidepoint(mouse_pos):
                if event.button == 1: # Left click
                    j_array = create_array(J_CENTER, J_ZOOM, J_SIZE)
                    J_CENTER = j_array[mouse_pos[0] - 50, mouse_pos[1] - 50]
                    draw_julia(screen, j_image)
                if event.button == 4: # Scroll up
                    j_array = create_array(J_CENTER, J_ZOOM, J_SIZE)
                    J_CENTER = j_array[mouse_pos[0] - 50, mouse_pos[1] - 50]
                    J_ZOOM = J_ZOOM * 1.5
                    # J_ITER = floor(100 * J_ZOOM)
                    draw_julia(screen, j_image)
                if event.button == 5: # Scroll down
                    j_array = create_array(J_CENTER, J_ZOOM, J_SIZE)
                    J_CENTER = j_array[mouse_pos[0] - 50, mouse_pos[1] - 50]
                    J_ZOOM = max(1, J_ZOOM / 1.5)
                    # J_ITER = floor(100 * J_ZOOM)
                    draw_julia(screen, j_image)
            if event.type == pg.MOUSEBUTTONUP and m_rect.collidepoint(mouse_pos):
                if event.button == 1: # Left click
                    m_array = create_array(M_CENTER, M_ZOOM, M_SIZE)
                    M_CENTER = m_array[mouse_pos[0] - 650, mouse_pos[1] - 50]
                    draw_mandelbrot(screen, m_image)
                if event.button == 3: # Right click
                    m_array = create_array(M_CENTER, M_ZOOM, M_SIZE)
                    C = m_array[mouse_pos[0] - 650, mouse_pos[1] - 50]
                    J_CENTER = 0
                    J_ZOOM = 1
                    draw_julia(screen, j_image)
                if event.button == 4: # Scroll up
                    m_array = create_array(M_CENTER, M_ZOOM, M_SIZE)
                    M_CENTER = m_array[mouse_pos[0] - 650, mouse_pos[1] - 50]
                    M_ZOOM = M_ZOOM * 1.5
                    # M_ITER = floor(100 * M_ZOOM)
                    draw_mandelbrot(screen, m_image)
                if event.button == 5: # Scroll down
                    m_array = create_array(M_CENTER, M_ZOOM, M_SIZE)
                    M_CENTER = m_array[mouse_pos[0] - 650, mouse_pos[1] - 50]
                    M_ZOOM = max(1, M_ZOOM / 1.5)
                    # M_ITER = floor(100 * M_ZOOM)
                    draw_mandelbrot(screen, m_image)
    pg.display.quit()

